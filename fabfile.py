import os

from fabric.api import local


def run():
    local('python manage.py runserver 0.0.0.0:8000')


def celery():
    local('celery -A proj worker --loglevel info')


def watch():
    local('gulp watch --dev')


def prepare_assets():
    try:
        os.mkdir('build')
    except OSError:
        pass
    local('python manage.py collectstatic --noinput')


def deploy():
    print('TODO')


def shell():
    local('python manage.py shell_plus')


def ws_srv():
    local('python -m apps.comet.ws-srv')
