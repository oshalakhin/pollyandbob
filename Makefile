run:
	export DJANGO_SETTINGS_MODULE=pollyandbob.settings_local; python manage.py runserver_plus

celery:
	export DJANGO_SETTINGS_MODULE=pollyandbob.settings_local; celery -A pollyandbob worker -B -l info
