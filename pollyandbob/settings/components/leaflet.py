LEAFLET_CONFIG = {
    'DEFAULT_CENTER': (51.165691, 10.451526),
    'RESET_VIEW': False,

    'DEFAULT_ZOOM': 4,
    'MIN_ZOOM': 3,
    'MAX_ZOOM': 17,
    'HEIGHT': 800,
    'PLUGINS': {
        'control-geocoder': {
            'css': ['leaflet/leaflet-control-geocoder-1.3.2/Control.Geocoder.css'],
            'js': 'leaflet/leaflet-control-geocoder-1.3.2/Control.Geocoder.js',
            'auto-include': True
        },
        'accurate-position': {
            'js': 'leaflet/Leaflet.AccuratePosition.js',
            'auto-include': True
        },
        'search': {
            'css': ['leaflet/leaflet-search/leaflet-search.css',
                    'leaflet/leaflet-search/leaflet-search.mobile.css'],
            'js': 'leaflet/leaflet-search/leaflet-search.js',
            'auto-include': True
        }
    }
}
