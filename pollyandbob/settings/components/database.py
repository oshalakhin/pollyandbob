DATABASES = {
    'default': {
         'ENGINE': 'django.contrib.gis.db.backends.postgis',
         'NAME': 'polly_bob',
         # 'USER': 'postgres',
         'ATOMIC_REQUESTS': True,
     }
}
