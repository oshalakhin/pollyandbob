from django.core.urlresolvers import reverse_lazy

AUTHENTICATION_BACKENDS = (
    'allauth.account.auth_backends.AuthenticationBackend',
    # 'comet.auth.TokenBackend',
    # 'comet.auth.VerifyBackend'
)

AUTH_USER_MODEL = 'users.User'

ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_USER_MODEL_USERNAME_FIELD = 'email'
ACCOUNT_EMAIL_VERIFICATION = 'mandatory'
ACCOUNT_EMAIL_SUBJECT_PREFIX = '[Polly&Bob] '
ACCOUNT_SIGNUP_PASSWORD_VERIFICATION = False
ACCOUNT_SIGNUP_FORM_CLASS = 'apps.users.forms.RegisterForm'
ACCOUNT_LOGIN_ON_EMAIL_CONFIRMATION = True
ACCOUNT_LOGIN_ON_PASSWORD_RESET = True

LOGIN_REDIRECT_URL = reverse_lazy('user_settings')
LOGIN_URL = reverse_lazy('login')
