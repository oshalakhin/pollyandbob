from split_settings.tools import optional, include

include(
    'components/*.py',

    # local settings (do not commit to version control)
    optional('local_settings.py'),

    scope=globals()
)
