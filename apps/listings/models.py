from django.conf import settings
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.gis.db import models
from django.core.urlresolvers import reverse
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from fluent_comments import get_model
from sorl.thumbnail import get_thumbnail

from apps.utils.models import ThumbnailMixin


class Category(models.Model):
    title = models.CharField(_("title"), max_length=255, db_index=True)
    order = models.PositiveIntegerField(_("order by"))

    class Meta:
        ordering = ['order']
        verbose_name = _("category")
        verbose_name_plural = _("categories")

    def __str__(self):
        return self.title


class Subcategory(models.Model):
    title = models.CharField(_("title"), max_length=255, db_index=True)
    order = models.PositiveIntegerField(_("order by"))
    category = models.ForeignKey(
        Category, related_name='subcategories', verbose_name=_("category"))

    class Meta:
        ordering = ['order']
        verbose_name = _('subcategory')
        verbose_name_plural = _('subcategories')

    def __str__(self):
        return self.title


class Listing(models.Model, ThumbnailMixin):
    title = models.CharField(_("Title"), max_length=120)
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    image = models.ImageField(_("Image"), upload_to='images')
    content = models.TextField(_("Content"))
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='listings',
        verbose_name=_("User"))
    category = models.ForeignKey(
        Category, related_name='listings', null=True,
        verbose_name=_("Category"))
    position = models.PointField(_("Geo position"), null=True)

    watchers = models.ManyToManyField(
        settings.AUTH_USER_MODEL, related_name='watched_listings')
    readers = models.ManyToManyField(
        settings.AUTH_USER_MODEL, related_name='unread', editable=False)

    comments = GenericRelation(get_model(), object_id_field='object_pk')

    class Meta:
        verbose_name = _("Listing")
        verbose_name_plural = _("Listings")

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.content
        super(Listing, self).save(*args, **kwargs)

    def get_absolute_url(self):
        # TODO fix when we have listing page
        return '/feed/'

    def to_list_display(self):
        return {'display_timestamp':self.created_at,
                'image': self.thumbnail_image,
                'title': self.title,
                'content': self.content,
                'category': self.category,
                'user': self.user,
                'url': reverse('pb_listing', kwargs={'listing_id': self.pk}),
                'position': self.position,
                'distance': self.distance.m}

    @property
    def heading_image(self):
        return get_thumbnail(self.image, '500x200', crop='top').url

    @property
    def large_image(self):
        return get_thumbnail(self.image, '1280x400', crop='top').url


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def set_categories_to_user(sender, instance, **kwargs):
    if kwargs['created']:
        instance.categories.add(*Category.objects.all())
