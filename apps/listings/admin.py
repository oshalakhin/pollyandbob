from django.contrib import admin
from django.core.urlresolvers import reverse

from .models import Category, Subcategory


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'subs', 'order')

    def subs(self, obj):
        soup = []
        for sub in obj.subcategories.all():
            soup.append('<a href="%s">%s (%s)</a><br>' % (reverse(
                'admin:jade_subcategory_change', args=(sub.id,)
            ), sub.value, sub.order))
        return ''.join(soup)
    subs.allow_tags = True
    subs.short_description = 'Subcategories'


admin.site.register(Category, CategoryAdmin)
admin.site.register(Subcategory)
