from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.gis.geos import Point
from django.utils.translation import ugettext_lazy as _

from .models import Listing


class ListingCreateForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self._creator = kwargs.pop('creator')
        super(ListingCreateForm, self).__init__(*args, **kwargs)

        watchers = get_user_model().objects\
            .exclude(id=self._creator.id)\
            .filter(position__distance_lte=self.distance_tup())\
            .order_by('title')
        watchers = watchers
        self.fields['watchers'].choices = [
            (watcher.id, watcher) for watcher in watchers]

    def clean_watchers(self):
        watchers = self.cleaned_data['watchers']
        # Only max of 100 neighbours can be chosen
        if watchers.count() > 100:
            raise forms.ValidationError(
                _('Only up to 100 neighbours can be chosen'))
        return watchers

    def around_position(self):
        return self._creator.position or Point(13.4533461, 52.5132939)

    def distance_tup(self):
        return (self.around_position(), settings.MAX_DISTANCE)

    def save(self, commit=True, *args, **kwargs):
        instance = super(ListingCreateForm, self).save(commit=False)
        instance.creator = self._creator

        if commit:
            instance.save()
            self.save_m2m()
        return instance

    class Meta:
        model = Listing
        fields = ['title', 'image', 'content', 'category', 'watchers']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': _('Add title. Max 30 characters')
            }),
            'content': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': _('Add description (max 200 characters)')
            })
        }


class FilterDistanceForm(forms.Form):
    distance = forms.FloatField()


class OptionToolCategoriesForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ['special_category_neighbors',
                  'special_category_groups',
                  'categories'
                  ]
