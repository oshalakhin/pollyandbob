from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import Group, GroupThread

#
# class CommentForm(forms.ModelForm):
#     """
#     Form to create group comment
#     """
#     thread = forms.IntegerField(required=False)
#
#     class Meta:
#         model = ThreadComment
#         fields = ['text', 'thread']
#
#     def __init__(self, *args, **kwargs):
#         self._author = kwargs.pop('author')
#         self._group = kwargs.pop('group')
#         super(CommentForm, self).__init__(*args, **kwargs)
#
#     def save(self, commit=True, *args, **kwargs):
#         instance = super().save(commit=False)
#         instance.author = self._author
#
#         if self.cleaned_data['thread']:
#             thread = GroupThread.objects.get(id=self.cleaned_data['thread'])
#         else:
#             thread = GroupThread(group=self._group)
#             thread.save()
#
#         instance.thread = thread
#
#         if commit:
#             instance.save()
#
#         return instance
#
#     def clean(self):
#         """
#         if group is closed and not in group raise error
#         """
#         is_in_group = self._author.is_in_group(self._group)
#         if not is_in_group and self._group.status == self.group.STATUS_CLOSED:
#             raise forms.ValidationError(_("You cannot comment in this group!"))


class UsersForm(forms.Form):
    """
    Form to get users to invite
    """
    users = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple)

    def __init__(self, *args, **kwargs):
        self._user = kwargs.pop('user')
        super(UsersForm, self).__init__(*args, **kwargs)
        self.fields['users'].choices = [(neighbour.id, neighbour.id) for
                                        neighbour in self._user.neighbours()]

    def clean_users(self):
        return [int(user) for user in self.cleaned_data['users']]


class GroupForm(forms.ModelForm):
    """
    Form to create/update group
    """

    class Meta:
        model = Group
        fields = ['status', 'content', 'title']
