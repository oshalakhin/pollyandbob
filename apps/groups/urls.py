from django.conf.urls import url

from .views import (
    BlockView, CommentView, CreateView, EditView, GroupView, InviteView,
    JoinView, LeaveView, UnblockView
)

app_name = 'group'
urlpatterns = [
    url(r'^(?P<pk>\d+)/$', GroupView.as_view(), name='page'),
    url(r'^(?P<pk>\d+)/block/$', BlockView.as_view(), name='block'),
    url(r'^(?P<pk>\d+)/unblock/$', UnblockView.as_view(), name='unblock'),
    url(r'^(?P<pk>\d+)/join/$', JoinView.as_view(), name='join'),
    url(r'^(?P<pk>\d+)/leave/$', LeaveView.as_view(), name='leave'),
    url(r'^(?P<pk>\d+)/comment/$', CommentView.as_view(), name='comment'),
    url(r'^(?P<pk>\d+)/invite/$', InviteView.as_view(), name='invite'),
    url(r'^(?P<pk>\d+)/edit/$', EditView.as_view(), name='edit'),
    url(r'^create/$', CreateView.as_view(), name='create'),
]
