from django.conf import settings
from django.contrib.gis.db import models
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail import get_thumbnail

from apps.utils.models import ThumbnailMixin


class Group(models.Model, ThumbnailMixin):
    STATUS_OPEN = 1
    STATUS_CLOSED = 2
    STATUS_SECRET = 3
    STATUS_CHOICES = (
        (STATUS_OPEN, _('Open')),
        (STATUS_CLOSED, _('Closed')),
        (STATUS_SECRET, _('Secret')),
    )

    title = models.CharField(_('Group name'), max_length=120)
    created_at = models.DateTimeField(_('Created at'),
                                      auto_now_add=True, null=True)
    status = models.IntegerField(
        _('Group status'), choices=STATUS_CHOICES, db_index=True,
        default=STATUS_OPEN, null=True)
    image = models.ImageField(_('Image file'), upload_to='images')
    content = models.TextField(_('Group description'))
    position = models.PointField(_('Geo location'), null=True)

    admins = models.ManyToManyField(
        settings.AUTH_USER_MODEL, related_name='administrated_groups',
        verbose_name=_('Group administrators'), blank=True)
    members = models.ManyToManyField(
        settings.AUTH_USER_MODEL, related_name='groups',
        verbose_name=_('Group members'), blank=True)
    blocked_by_users = models.ManyToManyField(
        settings.AUTH_USER_MODEL, related_name='blocked_groups', editable=False)

    class Meta:
        verbose_name = _("Group")
        verbose_name_plural = _("Groups")

    def __str__(self):
        return 'Group: %s' % self.name

    def get_absolute_url(self):
        return reverse('group:page', args=(self.id,))

    def to_list_display(self):
        category = {'url': '', 'value': 'Groups'}
        return {
            'category': category,
            'image': self.thumbnail_image,
            'title': self.name,
            'text': self.description,
            'creator':None,
            'url': reverse('pb_group', kwargs={'group_id':self.pk}),
            'position': self.position,
            'distance': self.distance.m,
        }

    @property
    def large_image(self):
        return get_thumbnail(self.image, '1280x400', crop='top').url


class GroupThread(models.Model):
    group = models.ForeignKey(
        Group, related_name='threads', verbose_name=_('Group'))
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name=_('Created at'))

    def __str__(self):
        return "Thread for group: %s" % self.group.name
