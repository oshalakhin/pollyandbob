from django.contrib.gis.db import models
from django.utils.translation import ugettext_lazy as _


class BaseImage(models.Model):
    """
    Honestly I didn't get where do we use it
    # ASK: does anybody know why do we need this model?
    """
    image = models.ImageField(_("image file"), upload_to='images')
    created_at = models.DateTimeField(
        _("created at"), auto_now_add=True, editable=False)
    content = models.TextField(_("image description"))

    class Meta:
        ordering = ['created_at']
        verbose_name = _("image")
        verbose_name_plural = _("images")


# class Language(models.Model):
#     code =