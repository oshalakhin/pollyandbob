"use strict";
/**
 * Created by goodfellow on 2/15/16.
 * This is a file to keep javascripts which have to work over the whole website
 *
 * PLEASE don't put scripts which have to work on a single page or app only
 */
//TODO: rewrite functionality to classes
//componentRestrictions: {
//      country: 'AU',
//      postalCode: '2000'
//    }
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", Cookies.get('csrftoken'));
        }
    }
});

$(function() {
    // make tabs in auth modal works from main menu
    $('.tab-toggle').on("click", function(){
      $('[href="#' + $(this).attr("data-target-tab") + '"]').tab('show');
    });

    // submit post forms with class "ajax_form" via ajax
    $("form.ajax_form[method='post']").submit(function (e) {
        e.preventDefault();
        $.post(
            $(this).attr('action'),
            $(this).serialize(),
            function (data) {
                if (data.hasOwnProperty('errors')) {
                    console.log(data.errors); //TODO: show errors, similar with example lower
                } else if (data.hasOwnProperty('location')) {
                    window.location = data.location;
                }
                console.log('successful post');
                //TODO: show messages (try to implement solution via django.contrib.messages)
            },
            "json"
        )
    });
    // we check zip of user so, we show him how long will he be able to use Polly&Bob for free
    $("#check_zip_form").on('submit', function(e) {
        e.preventDefault();
        var $form = this;
        $.get(
            $(this).attr('action'),
            $(this).serialize(),
            function(data) {
                if (data.hasOwnProperty('errors')) { // in case of form error
                    $($form).find('span.error').remove();
                    var i;
                    var errors = data.errors;
                    for (i in errors) {
                        var html = '';
                        var el = $($form).find("#id_" + i);
                        $.each(errors[i], function() {
                            html += '<span class="error">' + errors[i] + '</span>';
                        });
                        if (el.parent('div.sel_field').size()) {
                            el.parent().after(html);
                        } else {
                            el.after(html);
                        }
                    }
                } else if (data.hasOwnProperty('more_then_100')) {
                    if (data.more_then_100) {
                        $("#register_notice").text("Thanks for joining. Enjoy your three months free trial! With each neighbor you invite, you receive one month free usage. If you want to help to revive your neighborhood, please support us with only 2 € a month.");
                        $(".register_btn_outer .register_btn2").val("register 3 months trial");
                        //console.log('show 3 months trial form');
                    } else {
                        $("#register_notice").text("Congratulations! You are in the first 100 in you zip code area. Therefore we are happy to grant you 1 year of free usage! Isn't that awesome?");
                        $(".register_btn_outer .register_btn2").val("register 1 year free");
                        //console.log('show 1 year promo');
                    }
                    $('#register-modal').modal('show');
                }
            },
            "json"
        )
    });
    var autocomplete_options = {};
    if (!!$("id_country").val()) autocomplete_options = { 'componentRestrictions':
{ 'postalCode': $("id_zip_code").val() }, region: $("id_country").val() };
    //{'country': $("id_country").val()};
    $("#id_address").geocomplete(autocomplete_options);
      // .bind("geocode:result", function(event, result){
      //   $.log("Result: " + result.formatted_address);
      // })
      // .bind("geocode:error", function(event, status){
      //   $.log("ERROR: " + status);
      // })
      // .bind("geocode:multiple", function(event, results){
      //   $.log("Multiple: " + results.length + " results found");
      // });

    // $("#find").click(function(){
    //   $("#geocomplete").trigger("geocode");
    // });


    // $("#examples a").click(function(){
    //   $("#geocomplete").val($(this).text()).trigger("geocode");
    //   return false;
    // });
    // var autocomplete = new google.maps.places.Autocomplete($("#id_address")[0], {
    //     componentRestrictions: {
    //         country: 'DE'
    //         postalCode: '10623'
        // }
    // });
    // autocomplete.addListener('place_changed', function() {
    //     var place = autocomplete.getPlace();
    //     console.log(place.address_components);
    // });
    // hide errors if input has been changed
    $('select').on('change', function() {
        $(this).parent('div.sel_field').next('span.error').remove();
    });
    // or when we have focused on the input
    $('input').on('focus', function() {
        $(this).next('span.error').remove();
    });

    //map.on('map:loadfield', function (e) {
    //    // Customize map for field
    //  console.log(e.field, e.fieldid);
    //  alert('wtf');
    //});
    //$(window).on('map:init', function (e) {
    //  var detail = e.originalEvent ?
    //               e.originalEvent.detail : e.detail;
    //  var map = detail.map;
    //  L.Control.geocoder().addTo(map);
    //  map.findAccuratePosition({});
    //});
});
