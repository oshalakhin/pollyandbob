/**
 * Created by goodfellow on 11/30/15.
 */
$(function(){
    // show and hide special nodes on edit/save button click of ajax forms submition
    $(".ajax_form").submit(function (e) {
        $(this).find('div.msg_readable,textarea.msg,button.edit_btn,button.save_btn').toggleClass("hidden");
        $(this).find('div.msg_readable').text($(this).find('textarea.msg').val());
    });
    // toggle forms in profile page
    $(".edit_btn").on("click", function() {
        $(this).siblings('div.msg_readable,textarea.msg,button.save_btn').add(this).toggleClass("hidden");
    });
    // ask user if he's surely want to wipe out his profile
    $('button.delete_btn').on('click', function(e) {
        if (!confirm('Account will be deleted absolutely')) {
            e.preventDefault();
        }
    });
    // I don't like it, hope we gonna rename this "like_real_block_btn"
    $('.block_btn').on('click', function(e) {
        $(".like_real_block_btn").toggleClass("hidden");
    });

  if (L) {
    PositionField = L.GeometryField.extend({
      //_controlDrawOptions: function() {
      //  return {
      //      draw: {
      //          polyline: this.options.is_linestring,
      //          polygon: this.options.is_polygon,
      //          circle: false, // Turns off this drawing tool
      //          rectangle: this.options.is_polygon,
      //          marker: this.options.is_point
      //      }
      //  };
      //
      //},
      addTo: function (map) {
        var $this = this;
        var geocoder = new google.maps.Geocoder();
        function googleGeocoding(text, callResponse) {
          geocoder.geocode({address: text}, callResponse);
        }

        function formatJSON(rawjson) {
          var json = {},
            key, loc, disp = [];
          for(var i in rawjson)
          {
            key = rawjson[i].formatted_address;
            loc = L.latLng( rawjson[i].geometry.location.lat(), rawjson[i].geometry.location.lng() );
            var obj = {loc: loc, address_components: rawjson[i].address_components};
            json[ key ]= obj;	//key,value format
          }
          return json;
        }

        function filterD(text, records) {
          var frecords = [];
          for (var key in records) {
            for (var j in records[key].address_components) {
              if (records[key].address_components[j].types.indexOf("postal_code") + 1) {
                frecords[key] = records[key];
              }
            }
          }
          return frecords;
        }


        PositionSearch = L.Control.Search.extend({
          showLocation: function(latlng, title) {
            $this.drawnItems.clearLayers();
            $this.drawnItems.addLayer(
              L.GeoJSON.geometryToLayer({
                type: 'Point',
                coordinates: L.GeoJSON.latLngToCoords(latlng)
              })
            );
            $this.store.save($this.drawnItems);
            $this._setView();
          }
        });

        this.searchControl = new PositionSearch({
          sourceData: googleGeocoding,
          formatData: formatJSON,
          filterData: filterD,
          markerLocation: true,
          autoType: false,
          autoCollapse: true,
          minLength: 2,
          zoom: 10,
          circleLocation: false
        });
        map.addControl(this.searchControl);
        this.searchControl.on('search_locationfound', function(e) {
          var components = e.latlng.address_components;
          var postal_code, street_number, route, locality, country;
          for (var i in components) {
            if (components[i].types.indexOf("postal_code") + 1) {
              postal_code = components[i].short_name;
            }
            else if (components[i].types.indexOf("street_number") + 1) {
              street_number = components[i].short_name;
            }
            if (components[i].types.indexOf("route") + 1) {
              route = components[i].short_name;
            }
            if (components[i].types.indexOf("locality") + 1) {
              locality = components[i].short_name;
            }
            else if (components[i].types.indexOf("country") + 1) {
              country = components[i].short_name;
            }
          }
          $("#id_zip_code").val(postal_code);
          $("#id_city").val(locality);
          $("#id_country").val(country);
          $("#id_street").val(route + ' ' + street_number);
          //$("#id_position").text(L.GeoJSON.geometryToLayer({
          //  type: 'Point',
          //  coordinates: L.GeoJSON.latLngToCoords(e.latlng.loc)})
          //)
        });
        L.GeometryField.prototype.addTo.call(this, map);

        if (!this.store.formfield.value) {
          function onAccuratePositionProgress (e) {
            $this.drawnItems.clearLayers();
            $this.drawnItems.addLayer(L.GeoJSON.geometryToLayer({
                type: 'Point',
                coordinates: L.GeoJSON.latLngToCoords(e.latlng)
              })
            );
            $this._setView();
          }

          function onAccuratePositionError (e) {
              console.log(e.message)
          }

          function onAccuratePositionFound (e) {
            $this.drawnItems.clearLayers();
            $this.drawnItems.addLayer(
              L.GeoJSON.geometryToLayer({
                type: 'Point',
                coordinates: L.GeoJSON.latLngToCoords(e.latlng)
              })
            );
            $this._setView();
          }

          map.on('accuratepositionprogress', onAccuratePositionProgress);
          map.on('accuratepositionerror', onAccuratePositionError);
          map.on('accuratepositionfound', onAccuratePositionFound);
          map.findAccuratePosition({desiredAccuracy: 30 });
        }
        // Customize map for field
        //console.log(this);
        //function onAccuratePositionProgress (e) {
        //    console.log(e.accuracy);
        //    console.log(e.latlng);
        //}
        //
        //function onAccuratePositionError (e) {
        //    console.log(e.message)
        //}
        //
        //map.on('accuratepositionprogress', onAccuratePositionProgress);
        //map.on('accuratepositionfound', onAccuratePositionFound);
        //map.on('accuratepositionerror', onAccuratePositionError);
        //
        //map.findAccuratePosition({desiredAccuracy: 50 });
        //L.Control.geocoder().addTo(map);
      }
      //TODO: refactor
      //TODO: get address by point, find postal code there
      //TODO: check if user can change address in templates as well as in view
      //TODO: save data
      //TODO: filter addresses which contains postal code
    });
  }
});
