var jade = {
    data: {},
    developer: {
        reset: function() {
            $.cookie(jade.constants.seenWelcome, ''); // because false isn't false enough
        }
    },
    util: {
        fmt: {
            pad2: function(s) {
                s = '' + s;
                while (s.length < 2) s = '0' + s;
                return s;
            },

            fmtPair: function(pair, fmt) {
                return fmt(pair[0]) + ' - ' + fmt(pair[1])
            },

            minsToTime: function(mins) {
                return jade.util.fmt.pad2(Math.floor(mins/60)) + ':' + jade.util.fmt.pad2(mins%60);
            },

            time: function(pair) {
                return jade.util.fmt.fmtPair(pair, jade.util.fmt.minsToTime);
            },

            numToCupSize: function(n) {
                return ['AA','A','B','C','D','DD','F','G','H'][n];
            },

            cupSize: function(pair) {
                return jade.util.fmt.fmtPair(pair, jade.util.fmt.numToCupSize);
            },

            numToCm: function(n) {
                return n + 'cm';
            },

            cm: function(pair) {
                return jade.util.fmt.fmtPair(pair, jade.util.fmt.numToCm);
            },

            num: function(pair) {
                var id = function(n) {return n;};
                return jade.util.fmt.fmtPair(pair, id);
            }
        },
        onlyFirst: function(objectOrArray) {
            if ($.isArray(objectOrArray)) return objectOrArray[0];
            else return objectOrArray;
        },
        getParameter: function(name) {
            var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
            return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
        }
    },


    constants: {
        geo: {
            userLocation:'userLocation',
            gpsLocation:'gpsLocation',
            manualLocation:'manualLocation',
         },
        seenWelcome:'seenWelcome',
    },
    nav: {
        welcome: function() {
            if (! window.preventWelcome && ! $.cookie(jade.constants.seenWelcome)) {
                var cold = $('#cold-welcome');
                cold.modal(); // TODO: ('show') or what?
                cold.on('hide.bs.modal',function(e){
                    $.cookie(jade.constants.seenWelcome, true);
                });
                return true;
            }
            else return false;
        },
        addRouting: function(domID, x,y) {
            // link to OS maps
            var a = $(domID)[0];
            var url = '';
            var iOS = navigator.userAgent.match(/(iPad|iPhone|iPod)/g);
            var MacOS = navigator.appVersion.indexOf("Mac")!=-1;
            if (iOS || MacOS) {
                url = "http://maps.apple.com/?daddr=" + y + "," + x;
            }
            else {
                url = "http://maps.google.com/?daddr=" + y + "," + x;
            }
            a.href = url;
        },
        layoutProfile: function() {
            var profile = $('#profile-name-text');
            var pnLeft = 12;
            var headingWidth = pnLeft + profile.width();
            var widthRatio = headingWidth / $('#profile-modal').width();
            var fontSize = parseFloat(profile.css("font-size"));

            if (widthRatio > 0.85) {
                widthRatio *= 1.15;
                fontSize = Math.floor(fontSize/widthRatio);
                profile.css("font-size", fontSize);
            }

            // fit/position accent image
            var accent = $('#profile-name-accent');
            accent.width(Math.floor(fontSize/2.5));

            $('#profile-name').css('visibility','visible');

            var picContainer = $('#profile-pic-container');
            picContainer.css('width', picContainer.width()+'px');
            picContainer.css('height', picContainer.height()+'px');
        },
        galleryNext: function() {
            jade.data.galleryIndex = (jade.data.galleryIndex+1) % jade.data.galleryItems.length;
            var imageData = jade.data.galleryItems[jade.data.galleryIndex];
            var galleryContainer = $('#gallery-container');
            var oldGalleryImage = $('#gallery-container > img');
            var newGalleryImage = oldGalleryImage;
            oldGalleryImage.css('z-index', 1);
            newGalleryImage.css('z-index', 2);
            oldGalleryImage.fadeOut();
            newGalleryImage.fadeIn();
            //$('#gallery-container').appendChild;
        }
    },
    messages: {
        init: function() {
            $.views.converters('messagePopupClass', function(read) {
                if (!read) return 'message-popup-unread';
                else return '';
            });
        },
        setMessageOverview: function(messageList) {
            jade.messages.overview = messageList;
            jade.messages.map = {};
            if (!messageList || messageList.length == 0) return;
            var badgeTemplate = $.templates("#messagesBadgeTemplate");
            var popupTemplate = $.templates("#messagesPopupTemplate");
            var numUnread = 0;
            $.each(messageList, function(idx, message) {
                if (!message['read']) numUnread++;
                jade.messages.map[message.partner_pk] = idx;
            });

            if (numUnread > 0) {
                $('#messagesButton').addClass('highlighted');
                $('#messagesBadge').replaceWith(badgeTemplate.render({'unread':numUnread}));
            }
            else {
                $('#messagesButton').removeClass('highlighted');
                $('#messagesBadge').replaceWith('<span class="badge" id="messagesBadge"></span>')
            }
            var pc = popupTemplate.render({'messages':messageList});
            $('#messagesPopup').replaceWith(pc);
        }
    },
    forms: {
        init: function() {
            var csrfSafeMethod = function(method) {
                // these HTTP methods do not require CSRF protection
                return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
            };

            $.ajaxSetup({
                beforeSend: function(xhr, settings) {
                    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                        xhr.setRequestHeader("X-CSRFToken", $.cookie('csrftoken'));
                    }
                }
            });
        },
        upload: function(destination, fileData, postData, onSuccess, onError, onComplete) {
            if (!fileData) return;
            if (!postData) postData = {};
            var fd = new FormData();
            fd.append(fileData['name'], fileData['data']);
            $.each(postData, function(k, v) {fd.append(k, v);});
            $.ajax({
                url: destination,
                data: fd,
                processData: false,
                contentType: false,
                type: 'POST',
                success: onSuccess,
                error: onError,
                complete: onComplete
            });
        }
    },
    geo: {
        options: {
            enableHighAccuracy: false,
            timeout: 10000,
            maximumAge: 600000
        },
        getGeo: function() {
            console.log('getgeo');
            var result = {};
            $.cookie.json = true;
            $.each(jade.constants.geo, function(k, v) {
                result[k] = $.cookie(v);
            });
            return result;
        },
        getUserLocation: function() {
            return jade.geo.getGeo()[jade.constants.geo.userLocation];
        },
        init: function(force) {
            console.log('initgeo');
            var onReverseGeo = function(revResult) {
                console.log('reverse geo success');
                console.log(revResult);
                revResult = jade.util.onlyFirst(revResult);
                $.cookie.json = true;
                $.cookie(jade.constants.geo.gpsLocation,  revResult);
                $.cookie(jade.constants.geo.userLocation, revResult);
                setMap(revResult);
            };
            window.rGeocoderCb = onReverseGeo;

            var loc = jade.geo.getUserLocation();
            if (!!loc && !force) {
                console.log('geo caching, haha');
                setMap(loc);
            }
            else {
                if (!navigator.geolocation) {
                    console.log('geo denied');
                    $.removeCookie(jade.constants.geo.userLocation);
                    setMap(null);
                }
                else {
                    var onGeoSuccess = function(result) {
                        console.log('geo success');
                        console.log(result);
                        var crd = result.coords;
                        var src = 'http://nominatim.openstreetmap.org/reverse?json_callback=rGeocoderCb&format=json&lat=' + crd.latitude + '&lon=' + crd.longitude + '&zoom=27&addressdetails=1';
                        $('<script/>', {src:src}).appendTo('head');
                    };
                    var onGeoError = function(err) {
                        console.warn('ERROR(' + err.code + '): ' + err.message);
                        setMap(null);
                    };
                    navigator.geolocation.getCurrentPosition(onGeoSuccess, onGeoError, jade.geo.options);
                }
            }
        },
    },
};

