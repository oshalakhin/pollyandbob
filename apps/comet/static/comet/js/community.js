var community = {
    util: {
        genUUID4: function() {
            var d = new Date().getTime();
            var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = (d + Math.random()*16)%16 | 0;
                d = Math.floor(d/16);
                return (c=='x' ? r : (r&0x3|0x8)).toString(16);
            });
            return uuid;
        }
    },
    channels: {
        ws: {
            /**
             *
             * @param connectOptions endpoint: websocket endpoint, tokenEndpoint: rejuvenate token endpoint, token:token to send, handlers: message handlers
             * @param socketOptions: websockect connect options
             * @returns websocket
             */
            connect: function(connectOptions, socketOptions) {
                var socket = new ReconnectingWebSocket(connectOptions['endpoint'], null, socketOptions);
                socket.token = connectOptions['token'];
                socket.addHandler = function(key, handler) {
                    var handlersForKey = this.handlers[key];
                    if (handlersForKey) handlersForKey.push(handler);
                    else this.handlers[key] = [handler];
                };
                socket.onopen = function(event) {
                    this.send(this.token);
                };
                socket.onmessage = function(event) {
                    console.log(event.data);
                    var data = JSON.parse(event.data);
                    var that = this;
                    while(data.length >= 2) {
                        var key = data[0];
                        $(this.handlers[key]).each(function(idx, handler){
                            handler.call(that, data[1]);
                        });
                        data = data.slice(2);
                    }
                };
                var handlers = connectOptions['handlers'];
                if (handlers) {
                    socket.handlers = handlers;
                }
                else {
                    socket.handlers = {};
                    socket.addHandler('expire', function() {
                        var handlers = this.handlers;
                        socket.close();
                        // rejuvenate token/socket
                        $.post(connectOptions['tokenEndpoint'],{})
                            .done(function( resultDict, textStatus, jqXHR ) {
                                connectOptions['handlers'] = handlers;
                                connectOptions['token'] = resultDict['data']['token'];
                                var newSocket = community.ws.connect(connectOptions, socketOptions);
                                if (window.websocket) {
                                    window.websocket = newSocket;
                                }
                            })
                            .fail(function( jqXHR, textStatus, errorThrown ) {
                                console.error('failed to rejuvenate', textStatus, errorThrown);
                            });
                    });
                }

                socket.open();
                return socket;
            }
        },
        js: {
            connect: function(onReceive) {
                var makeHandler = function() {
                    var handler = {
                        bridge: null,
                        sendQueue: [],
                        processQueue: function() {
                            if (!this.bridge) return;
                            var that = this;
                            $(this.sendQueue).each(function(idx, obj) {
                                var data = obj[0];
                                var callback = obj[1];
                                if (callback) that.bridge.send(data, callback);
                                else that.bridge.send(data);
                            });
                        },
                        send: function(data, callback) {
                            this.sendQueue.push([data, callback]);
                            this.processQueue();
                        },
                        onConnect: function() {
                            this.processQueue();
                        },
                        onReceiveInternal: function(message, responseCallback) {
                            console.log('[framework] Received message: ' + message);
                            onReceive(message);
                            if (responseCallback) responseCallback();
                        }
                    };

                    return handler;
                };

                var connectIOSbridge = function(onConnect) {
                    if (window.WebViewJavascriptBridge) {
                        onConnect(WebViewJavascriptBridge)
                    } else {
                        document.addEventListener('WebViewJavascriptBridgeReady', function() {
                            onConnect(WebViewJavascriptBridge)
                        }, false)
                    }
                };

                // TODO: android bridge
                var handler = makeHandler();

                connectIOSbridge(function(bridge) {
                    console.log('got a jsbridge', bridge);
                    bridge.init(handler.onReceiveInternal);
                    handler.bridge = bridge;
                    handler.onConnect();
                });

                return handler;
            }
        }
    }
};
