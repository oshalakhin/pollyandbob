from django.contrib import admin

from .models import Token, Verify

admin.site.register([Token, Verify])
