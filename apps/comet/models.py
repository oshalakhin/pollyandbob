import uuid
import structlog

from django.conf import settings
from django.contrib.gis.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django_cleanup.signals import cleanup_pre_delete

logger = structlog.get_logger()


class TokenQuerySet(models.QuerySet):
    def ids(self):
        return ','.join(o[0].hex for o in self.values_list('id'))


class Token(models.Model):
    objects = TokenQuerySet.as_manager()
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)

    @classmethod
    def expire_secs(cls):
        return getattr(settings, 'WS_AUTHTOKEN_EXPIRE_SECS', 3600)

    @classmethod
    def is_immortal(cls):
        return Token.expire_secs() <= 0

    @classmethod
    def for_user(cls, user):
        result = None
        try:
            existing = Token.objects.filter(user=user).order_by('-created_at')[0:1].get()
            if Token.is_immortal() or existing.lifetime_left_secs > Token.expire_secs() / 2:
                result = existing
        except Token.DoesNotExist:
            pass
        if not result:
            result = Token.objects.create(user=user)
        return result

    @property
    def lifetime_left_secs(self):
        if Token.is_immortal():
            return None
        return Token.expire_secs() - (timezone.now() - self.created_at).total_seconds()

    @property
    def is_valid(self):
        return Token.is_immortal() or self.lifetime_left_secs > 0


class Verify(models.Model):
    VERIFY_PHONE = 'p'
    VERIFY_EMAIL = 'e'
    VERIFY_CHOICES = (
        (VERIFY_PHONE, 'Phone'),
        (VERIFY_EMAIL, 'Email'),
    )
    mode = models.CharField(max_length=1, choices=VERIFY_CHOICES)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='verify')

    @classmethod
    def create(cls, user):
        result = cls(user=user)
        result.mode = Verify.VERIFY_PHONE if user.phone else Verify.VERIFY_EMAIL
        result.save()
        return result

    def __str__(self):
        return "Verify code %s for %s" % (self.id, self.user)

    class Meta:
        verbose_name_plural = _('Verifies')


def sorl_delete(**kwargs):
    from sorl.thumbnail import delete
    delete(kwargs['file'])
cleanup_pre_delete.connect(sorl_delete)
