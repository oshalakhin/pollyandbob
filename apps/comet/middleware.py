# ASK
import structlog
from django.contrib import auth
from django.contrib.auth import get_user_model
from django.contrib.auth.middleware import RemoteUserMiddleware
from django.core.exceptions import ImproperlyConfigured

from .api_messages import API_ERR_UNAUTHORIZED
from .lib import api_response_err

logger = structlog.get_logger()


class ExceptionLoggingMiddleware(object):
    def process_exception(self, request, exception):
        logger.exception("BOOM %s", request.path)


class StructLogMiddleware(object):
    def process_request(self, request):
        user = 'None'
        if hasattr(request.user, 'email'):
            user = request.user.email
        logger.new(
            user=user,
            path=request.path
        )


class AutoAnonUserMiddleware(object):
    def process_view(self, request, callback, callback_args, callback_kwargs):
        if not hasattr(request, 'user'):
            raise ImproperlyConfigured("Need django.contrib.auth middleware")
        if not getattr(callback, 'auto_user_exempt', False) and \
                not request.user.is_authenticated():

            user = get_user_model()()
            password = user.__class__.make_random_password()
            user.set_password(password)
            user.save()
            user = auth.authenticate(email=user.email, password=password)
            auth.login(request, user)


class TokenAuthMiddleware(RemoteUserMiddleware):
    def process_request(self, request):
        token_id = request.META.get('HTTP_X_AUTH_TOKEN')
        if not token_id:
            return
        try:
            user = auth.authenticate(token_id=token_id)
            # Adapted from RemoteUserMiddleware - not sure if we need this.
            if request.user.is_authenticated():
                if request.user.get_username() == self.clean_username(user.email, request):
                    return
                else:
                    # An authenticated user is associated with the request, but
                    # it does not match the authorized user in the header.
                    self._remove_invalid_user(request)

            # logged in via header -> cannot be forged in browsers -> do not need or want csrf check
            if user and user.is_authenticated():
                request.csrf_processing_done = True
                auth.login(request, user)
            else:
                raise Exception
        except:
            return api_response_err(**API_ERR_UNAUTHORIZED)
