from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from push_notifications.models import APNSDevice, GCMDevice
from rest_framework.authentication import SessionAuthentication
from rest_framework.decorators import api_view, authentication_classes

from apps.comet import lib as clib
from apps.comet.models import Token


class CSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        pass


@api_view(['GET'])
@login_required
def token_info(request):
    token_id = request.META.get('HTTP_X_AUTH_TOKEN')
    token = Token.objects.get(pk=token_id)
    return clib.api_response_ok({'time_left':token.lifetime_left_secs})


@api_view(['POST'])
@authentication_classes((CSessionAuthentication,))
def open(request):
    if request.user and request.user.is_authenticated():
        user = request.user
    else:
        user = authenticate(email=request.POST.get('email'), password=request.POST.get('password'))
    if user is None:
        return clib.api_response_err(**clib.API_ERR_USER_UNKNOWN)
    if not user.is_active:
        return clib.api_response_err(**clib.API_ERR_ACCOUNT_LOCKED)
    new_token = Token.for_user(user)
    push_token = request.POST.get('apns_push_token', None)
    if push_token is not None:
        APNSDevice.objects.update_or_create(user=user, registration_id=push_token)

    push_token = request.POST.get('gcm_push_token', None)
    if push_token is not None:
        GCMDevice.objects.create(user=user, registration_id=push_token)
    result = {'token':new_token.id.hex, 'time_left':new_token.lifetime_left_secs}
    return clib.api_response_ok(result)


# TODO auth via shared secret?
@api_view(['GET'])
def token_valid(request, token):
    try:
        Token.objects.get(pk=token)
        return clib.api_response_ok(True)
    except Token.NotFound:
        return clib.api_response_ok(False)
