from django.conf import settings

from apps.comet.models import Token


def ws_processor(request):
    result = {'tokenEndpoint': '/open', 'wsEndpoint':settings.CB_WS_URL.replace('http','ws')}
    if request.user and request.user.is_authenticated():
        result['token'] = Token.for_user(request.user)
    return result
