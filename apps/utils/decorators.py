# -*- coding: utf-8 -*-
from functools import wraps

from django.utils.decorators import available_attrs


def auto_user_exempt(view_func):
    """
    Marks a view function as being exempt from automatically getting a
     materialized anon user, bound to their session
    """
    def wrapped_view(*args, **kwargs):
        return view_func(*args, **kwargs)
    wrapped_view.auto_user_exempt = True
    return wraps(view_func, assigned=available_attrs(view_func))(wrapped_view)
