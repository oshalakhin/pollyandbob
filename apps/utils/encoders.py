# -*- coding: utf-8 -*-
from django.contrib.gis.geos import Point
from django.core.serializers.json import DjangoJSONEncoder


class SmartJSONEncoder(DjangoJSONEncoder):
    """
    Handle Point objects too
    """
    def default(self, o):
        if isinstance(o, Point):
            return "{0},{1}".format(o.y, o.x)
        else:
            return super(SmartJSONEncoder, self).default(o)
