from sorl.thumbnail import get_thumbnail

# TODO to_list_display_attributes_merge {image: ...}

LISTING_THUMBNAIL_GEOMETRY = '200x135'


class ThumbnailMixin:

    @property
    def thumbnail_image(self):
        if not self.image:
            return None
        return get_thumbnail(
            self.image, LISTING_THUMBNAIL_GEOMETRY, crop='left').url


nb = dict(null=True, blank=True)
