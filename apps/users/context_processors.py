from allauth.account.forms import LoginForm, SignupForm

from .forms import PostalCodeForm


def common_forms(request):
    result = {}
    if request.user.is_authenticated:
        result['postal_code_form'] = PostalCodeForm()
        result['register_form'] = SignupForm()
        result['login_form'] = LoginForm()
    return result
