from django import forms
from django.contrib.auth import get_user_model
from django.forms.widgets import Input
from django.utils.translation import ugettext_lazy as _
from django_countries.widgets import CountrySelectWidget
from leaflet.forms.widgets import LeafletWidget


class ChangePasswordForm(forms.ModelForm):
    old_password = forms.CharField(
        widget=forms.PasswordInput, label=_('Alt passwort'))
    new_password = forms.CharField(
        30, 6, widget=forms.PasswordInput, label=_('Neu passwort'))
    new_password2 = forms.CharField(
        30, 6, widget=forms.PasswordInput, label=_('Neu passwortwiederholung'))

    class Meta:
        model = get_user_model()
        fields = []

    def clean_old_password(self):
        old_password = self.cleaned_data.get('old_password')
        if not self.instance.check_password(old_password):
            raise forms.ValidationError("Password incorrect")
        return old_password

    def clean_new_password2(self):
        # Check that the two password entries match
        new_password = self.cleaned_data.get('new_password')
        new_password2 = self.cleaned_data.get('new_password2')
        if new_password and new_password2 and new_password != new_password2:
            raise forms.ValidationError("Passwords don't match")
        return new_password2

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['new_password'])
        if commit:
            user.save()
        return user


class ChangeEmailForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ['email']

    def save(self, commit=True):
        super(ChangeEmailForm, self).save(commit)
        #TODO: send activation email


class DeleteAccountForm(forms.Form):
    delete = forms.RadioSelect(choices=['no', 'yes'])


class EmailNotificationsForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ['notify_me_never', 'notify_on_changes_in_connection_center',
                  'notify_on_invite_to_listing', 'sent_me_newsletter']


class EmailForm(forms.Form):
    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'placeholder': _('E-Mail of a friend'),
        'class': 'invite_field'
    }))


class LanguageForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ['language']


class PositionWidget(LeafletWidget):
    geometry_field_class = 'PositionField'

    def serialize(self, value):
        return value.geojson if value else ''

    def render(self, name, value, attrs=None):
        return super().render(name, value, attrs)


class AddressForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ['zip_code', 'country', 'position']
        widgets = {'position': PositionWidget()}

    def zip_code__clean(self):
        if self.model.filter(
                zip_code=self.cleaned_data['zip_code']).count() < 100:
            return super(AddressForm, self).zip_code__valid()
        raise forms.ValidationError(_('Only first 100 users in one zip are '
                                      'able to register for free'))

    def clean(self):
        if self.instance.can_change_location():
            return super(AddressForm, self).clean()
        raise forms.ValidationError(_('You can change location only every 6 '
                                      'months if you are not on PRO account'))


class RegisterForm(forms.Form):
    field_order = ('title', 'address', 'email', 'password')

    address = forms.CharField(300, label=_('Address'))

    def signup(self, request, user):
        user.save()


class PostalCodeForm(forms.ModelForm):

    class Meta:
        model = get_user_model()
        fields = ['zip_code', 'country']
        widgets = {
            'country': CountrySelectWidget(
                    attrs={'class': 'select_field te_up2'}),
            'zip_code': Input(
                    attrs={'class': 'register_field te_up',
                           'placeholder': "Postalcode Area"})
        }

    def clean_zip_code(self):
        zip_code = self.cleaned_data.get('zip_code', None)
        if not zip_code:
            raise forms.ValidationError("Please enter your postalcode Area")
        return zip_code

    def clean_country(self):
        country = self.cleaned_data.get('country', None)
        if not country:
            raise forms.ValidationError("Please enter your Country")
        return country


# class PasswordField(forms.CharField):
#     def __init__(self, *args, **kwargs):
#         render_value = kwargs.pop(
#                 'render_value')#, app_settings.PASSWORD_INPUT_RENDER_VALUE)
#         kwargs['widget'] = forms.PasswordInput(
#             render_value=render_value,
#             attrs={'placeholder': _(kwargs.get("label")),
#                    'class': 'register_field te_up'})
#         super(PasswordField, self).__init__(*args, **kwargs)


# class CustomLoginForm(LoginForm):
#     password = forms.CharField(label=_("Password"))
#
#     def __init__(self, *args, **kwargs):
#         super(LoginForm, self).__init__(*args, **kwargs)
#         login_widget = forms.TextInput(attrs={
#             'type': 'email',
#             'placeholder': _('E-mail'),
#             'autofocus': 'autofocus',
#             'class': 'register_field te_up'
#         })
#         login_field = forms.EmailField(label=_("E-mail"),
#                                        widget=login_widget)
#         self.fields["login"] = login_field
        # set_form_field_order(self,  ["login", "password", "remember"])
        # if app_settings.SESSION_REMEMBER is not None:
        #     del self.fields['remember']
