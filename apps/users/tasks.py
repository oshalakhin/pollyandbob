from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string
from pollyandbob.celery import app


@app.task
def send_email(subject, message_template, to, context={}):
    """
    Send email message
    """
    send_mail(
        subject=subject,
        message=render_to_string(message_template, context),
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[to],
        fail_silently=False
    )
