from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.gis.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from fluent_comments import get_model

from apps.utils.models import nb


class Notification(models.Model):
    TYPE_JOIN_REQUEST = 'join-request'
    TYPE_REPLY = 'reply'
    TYPE_COMMENT = 'comment'
    TYPE_CHOICES = (
        (TYPE_JOIN_REQUEST, _('Join request')),
        (TYPE_COMMENT, _('Comment')),
        (TYPE_REPLY, _('Reply')),
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,  # ALEX: why do we need to keep notification after user deletion?
        verbose_name=_('User'), null=True, related_name='notifications',
        db_index=True)

    notification_type = models.CharField(
        _('Type'), choices=TYPE_CHOICES, max_length=255, null=True,
        db_index=True)
    initiator = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='notification_initiator',
        db_index=True, **nb)
    listing = models.ForeignKey('listings.Listing', db_index=True, **nb)
    # comment = models.ForeignKey(get_model())
    group = models.ForeignKey('groups.Group', db_index=True, **nb)

    is_watched = models.BooleanField(_('Viewed'), default=False, db_index=True)
    created_at = models.DateTimeField(
        _('Creation date'), auto_now_add=True, null=True)
    watched_at = models.DateTimeField(_('Viewed date'), **nb)

    def __str__(self):
        return '%s %s' % (self.user, self.get_notification_type_display())

    @property
    def cover(self):
        if self.listing:
            return self.listing.image

        return self.group.image

    @property
    def title(self):
        if self.listing:
            return self.listing.title

        return self.group.name

    def get_absolute_url(self):
        # TODO will add appropriate url later
        return '/'

    class Meta:
        ordering = ['-watched_at']
        verbose_name = _('Notification')
        verbose_name_plural = _('Notifications')


# We have to put notification logic to notifications app even if
# it's relating to comments
@receiver(post_save, sender=get_model())
def notify_on_comment(sender, instance, **kwargs):
    """
    Get users list that must be notified and create notifications for them
    """
    users = get_user_model().objects\
        .filter(id__in=instance.group.members.values_list('id', flat=True))
    # authors = instance.admins.all()

    Notification.objects.bulk_create([
        Notification(
            user=user,
            initiator=instance.author,
            group=instance.group,
            notification_type=Notification.TYPE_COMMENT,
            comment=instance) for user in users])
#     users = get_user_model().objects\
#         .filter(id__in=instance.listing.watchers.values_list('id', flat=True))
#     authors = get_user_model().objects.filter(pk=instance.listing.creator.id)
#
#     Notification.objects.bulk_create([
#         Notification(
#             user=user,
#             initiator=instance.author,
#             listing=instance.listing,
#             notification_type=Notification.TYPE_COMMENT,
#             listing_comment=instance) for user in users | authors])
