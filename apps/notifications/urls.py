from django.conf.urls import url

from apps.notifications.views import ConnectionCenter

urlpatterns = [

    url(r'^connection-center/$',
        ConnectionCenter.as_view(), name='connection_center'),
]
