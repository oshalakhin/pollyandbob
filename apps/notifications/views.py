from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView

from apps.notifications.models import Notification


class ConnectionCenter(LoginRequiredMixin, TemplateView):
    """
    Connection center for notifications
    """
    template_name = "notifications/connection_center.html"

    def get_objects(self):
        return Notification.objects.filter(user=self.request.user)

    def get_context_data(self, **kwargs):
        kwargs.update({
            'notifications': self.get_objects()
        })
        return super(ConnectionCenter, self).get_context_data(**kwargs)
