from django.conf import settings
from django.contrib.gis.db import models
from django.utils.translation import ugettext_lazy as _


class Achievement(models.Model):
    title = models.CharField(
        _("Achievement title"), blank=False, null=False, max_length=40)
    points = models.PositiveIntegerField(_("Points"))
    users = models.ManyToManyField(
        settings.AUTH_USER_MODEL, through='AchievementAward',
        verbose_name=_("Users achieved it"), related_name='achievements')

    class Meta:
        verbose_name = _("Achievement")
        verbose_name_plural = _("Achievements")

    def __str__(self):
        return self.title


class AchievementAward(models.Model):
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    achievement = models.ForeignKey(Achievement, verbose_name=_("Achievement"))
    user = models.ForeignKey(settings.AUTH_USER_MODEL)

    class Meta:
        verbose_name = _("Achievement award")
        verbose_name_plural = _("Achievement awards")

    def __str__(self):
            return "%s by %s" % (self.achievement, self.user)
